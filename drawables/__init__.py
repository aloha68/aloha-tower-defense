#!/usr/bin/env python3

FPS = 60
BLOCK_SIZE = 32


# Fonction de callback pour le débogage
debug_callback = None


def init_debug(callback):
    global debug_callback
    debug_callback = callback


def debug(msg):
    global debug_callback

    if callable(debug_callback):
        debug_callback(msg)
    else:
        print("Drawables:", msg)