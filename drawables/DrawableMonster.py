#!/usr/bin/env python3

from drawables import *
from drawables.Drawable import MovableDrawable

import os
import random
import pygame


class DrawableMonster(MovableDrawable):
    """Représente un monstre"""

    def __init__(self, monster, *groups):

        self.monster_type = monster['monster_type']
        self.name = monster['name']
        self.speed = monster['speed']
        self.hit_points = monster['hitpoints']
        self.entrance = monster['entrance']
        self.exit = monster['exit']

        MovableDrawable.__init__(self, self.entrance, self.get_image(), *groups)

        # Ajustement de la vitesse
        self.speed = self.speed / FPS
        if self.speed < 1:
            self.speed = 1
        elif self.speed > 2:
            self.speed = 2

        self.direction = 'right'
        self.look_side = 'right'
        self.current_position = ()

        debug("Monster {} appears in {}".format(self.name, self.entrance))

    def get_image(self):
        """Retounre l'image adaptée au monstre"""

        images = []
        for f in os.listdir('images/monsters/'):
            if f.startswith('emotion_'):
                images.append(f)

        return 'monsters/' + random.choice(images)

    def update(self):
        """
        Met à jour la position du monstre
        Méthode utilisée par le framework pygame pour obtenir la nouvelle position du sprite
        Pour cela, mettre à jour la propriété self.rect
        """

        # On exécute le monstre si nécessaire
        if self.hit_points <= 0:
            self.kill()
            return

    def is_exit(self, exit_blocks):
        """Vérifie si le monstre se trouve sur un bloc de fin"""
        return pygame.sprite.spritecollideany(self, exit_blocks)

    def update_position(self, blocks):
        """Vérifie si le monstre entre en collision avec un bloc"""

        # Variables nécessaires pour le calcul de la prochaine position
        self.last_direction = self.direction
        self.current_position = self.rect.topleft

        availables_directions = self.get_availables_directions()

        nbTry = 1
        while 1:

            try:
                self.direction = availables_directions.pop()
            except IndexError:
                raise Exception("Problem with monster pathing!")

            self.rect.topleft = self.get_new_position()

            # On regarde si le monstre rencontre un bloc du décor
            block = pygame.sprite.spritecollideany(self, blocks)
            if block is None:

                if self.direction != self.last_direction:
                    self.change_direction()
                break

            # debug("Collide{}: {} in {} with {}".format(nbTry, self.direction, self.current_position, block.rect))
            nbTry += 1

    def change_direction(self):
        """Change la direction du monstre et adapte son image"""

        # debug("Changement de direction: %s" % self.direction)

        if self.direction == 'right' and self.look_side != 'right':
            self.look_side = 'right'
            self.image = pygame.transform.flip(self.image, 1, 0)
        elif self.direction == 'left' and self.look_side != 'left':
            self.look_side = 'left'
            self.image = pygame.transform.flip(self.image, 1, 0)

    def get_availables_directions(self):
        """Retourne une liste de direction disponible pour le monstre"""

        directions = ['up', 'down', 'left', 'right']
        directions.remove(self.get_opposite_direction(self.direction))

        random.shuffle(directions)
        return directions

    def get_new_position(self):
        """
        Retourne la nouvelle position suite à un mouvement.
            self.speed doit contenir la vitesse actuelle du monstre
            self.direction doit contenir la direction actuelle du monstre
        """

        movement = None

        if self.direction == 'up':
            movement = (0, 0 - self.speed)
        elif self.direction == 'down':
            movement = (0, self.speed)
        elif self.direction == 'left':
            movement = (0 - self.speed, 0)
        elif self.direction == 'right':
            movement = (self.speed, 0)

        if movement is not None:
            return [sum(x) for x in zip(self.current_position, movement)]

